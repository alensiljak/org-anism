var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const oast_to_hast_1 = __importDefault(require("oast-to-hast"));
function reorg2rehype(options) {
    return transformer;
    function transformer(node) {
        return oast_to_hast_1.default(node, options);
    }
}
module.exports = reorg2rehype;
//# sourceMappingURL=index.js.map