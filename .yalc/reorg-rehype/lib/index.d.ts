import { Options } from 'oast-to-hast';
import { Transformer } from 'unified';
declare function reorg2rehype(options: Partial<Options>): Transformer;
export = reorg2rehype;
