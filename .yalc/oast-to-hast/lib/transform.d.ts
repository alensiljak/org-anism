import { Node } from 'unist';
import u from 'unist-builder';
import { HNode } from './';
export declare const one: <C extends {
    h: (tagName: string, properties?: import("hast").Properties) => (...children: HNode[]) => HNode;
    u: typeof u;
    excludeTags: string[];
    selectTags: string[];
    highlight: boolean;
    properties: import("hast").Properties;
    handlers: {
        [key: string]: import("./handlers").Handler;
    };
}>(context: C) => (node: Node) => HNode;
export declare const all: <C extends {
    h: (tagName: string, properties?: import("hast").Properties) => (...children: HNode[]) => HNode;
    u: typeof u;
    excludeTags: string[];
    selectTags: string[];
    highlight: boolean;
    properties: import("hast").Properties;
    handlers: {
        [key: string]: import("./handlers").Handler;
    };
}>(context: C) => (nodes: Node[]) => HNode[];
