import { Block } from 'orga';
import { Context, HNode } from '../';
declare const _default: (context: Context) => (node: Block) => HNode;
export default _default;
