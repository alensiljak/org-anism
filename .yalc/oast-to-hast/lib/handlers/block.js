var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const _highlight_1 = __importDefault(require("./_highlight"));
exports.default = (context) => (node) => {
    const { h, u } = context;
    const name = node.name.toLowerCase();
    if (name === 'src') {
        let body = u('text', node.value);
        const lang = node.params[0];
        if (lang && context.highlight) {
            body = u('raw', _highlight_1.default(lang, node.value));
        }
        return h('pre')(h('code', { className: [`language-${lang}`] })(body));
    }
    switch (name) {
        case 'quote':
            return h('blockquote')(u('text', node.value));
        case 'center':
            return h('center')(u('text', node.value));
        case 'export':
            return u('raw', node.value);
        case 'comment':
        default:
            return h('pre', { className: [name] })(u('text', node.value));
    }
};
//# sourceMappingURL=block.js.map