Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (context) => (node) => {
    if (node.key.toLowerCase() === 'select_tags') {
        const tags = node.value.split(' ').map(v => v.trim()).filter(Boolean);
        context.selectTags = tags;
    }
    return undefined;
};
//# sourceMappingURL=keyword.js.map