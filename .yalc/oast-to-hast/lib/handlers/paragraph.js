Object.defineProperty(exports, "__esModule", { value: true });
const transform_1 = require("../transform");
exports.default = (context) => (node) => {
    const { h } = context;
    const properties = node.attributes.attr_html;
    return h('p', properties)(...transform_1.all(Object.assign(Object.assign({}, context), { properties }))(node.children));
};
//# sourceMappingURL=paragraph.js.map