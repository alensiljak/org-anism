import { Headline } from 'orga';
import { Context, HNode } from '../';
declare const _default: (context: Context) => (node: Headline) => HNode;
export default _default;
