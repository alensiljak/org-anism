Object.defineProperty(exports, "__esModule", { value: true });
exports.table = exports.tableCell = exports.tableRow = void 0;
const transform_1 = require("../transform");
const tableRow = (context) => (node) => {
    return context.h('tr')(...transform_1.all(context)(node.children));
};
exports.tableRow = tableRow;
const tableCell = (context) => (node) => {
    const { isHead, h } = context;
    return h(isHead ? 'th' : 'td')(...transform_1.all(context)(node.children));
};
exports.tableCell = tableCell;
const table = (context) => (node) => {
    const { h, u } = context;
    let nodes = [];
    let children = node.children;
    let hrCount = 0;
    while (children.length > 0) {
        const i = children.findIndex(n => n.type === 'table.hr');
        const index = i > 0 ? i : children.length;
        const chunk = children.slice(0, index);
        children = children.slice(index + 1);
        const isHead = hrCount === 0 && i > 0;
        nodes.push(h(isHead ? 'thead' : 'tbody')(...transform_1.all(Object.assign(Object.assign({}, context), { isHead }))(chunk)));
        hrCount += 1;
    }
    const tableProperties = Object.assign({ border: 2, cellspacing: 0, cellpadding: 6, rules: 'groups', frame: 'hsides' }, node.attributes.attr_html);
    return h('table', tableProperties)(...nodes, h('caption')(u('text', node.attributes.caption || '')));
};
exports.table = table;
//# sourceMappingURL=table.js.map