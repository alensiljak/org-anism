import { Footnote, FootnoteReference } from 'orga';
import { Context, HNode } from '../';
export declare const footnoteReference: ({ h, u }: Context) => (node: FootnoteReference) => HNode;
export declare const footnote: (context: Context) => (node: Footnote) => HNode;
