import { HTML } from 'orga';
import { Context, HNode } from '../';
declare const _default: (context: Context) => (node: HTML) => HNode;
export default _default;
