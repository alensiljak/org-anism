var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prismjs_1 = __importDefault(require("prismjs"));
const components_1 = __importDefault(require("prismjs/components"));
const getBaseLanguageName = (nameOrAlias, components = components_1.default) => {
    if (components.languages[nameOrAlias]) {
        return nameOrAlias;
    }
    return Object.keys(components.languages).find(language => {
        const { alias } = components.languages[language];
        if (!alias)
            return false;
        if (Array.isArray(alias)) {
            return alias.includes(nameOrAlias);
        }
        else {
            return alias === nameOrAlias;
        }
    });
};
function loadPrismLanguage(language) {
    const baseLanguage = getBaseLanguageName(language);
    if (!baseLanguage) {
        throw new Error(`Prism doesn't support language '${language}'.`);
    }
    if (prismjs_1.default.languages[baseLanguage]) {
        return;
    }
    const languageData = components_1.default.languages[baseLanguage];
    if (languageData.option === `default`) {
        return;
    }
    if (languageData.require) {
        if (Array.isArray(languageData.require)) {
            languageData.require.forEach(loadPrismLanguage);
        }
        else {
            loadPrismLanguage(languageData.require);
        }
    }
    require(`prismjs/components/prism-${baseLanguage}.js`);
}
exports.default = loadPrismLanguage;
//# sourceMappingURL=_load-prism-language.js.map