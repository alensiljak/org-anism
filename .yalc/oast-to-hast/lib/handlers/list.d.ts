import { List, ListItem, ListItemCheckbox } from 'orga';
import { Context, HNode } from '../';
declare const _default: (context: Context) => (node: List) => HNode;
export default _default;
export declare const item: (context: Context) => (node: ListItem) => HNode;
export declare const checkbox: ({ h }: Context) => (node: ListItemCheckbox) => HNode;
