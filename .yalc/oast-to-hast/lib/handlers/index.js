var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const keyword_1 = __importDefault(require("./keyword"));
const block_1 = __importDefault(require("./block"));
const footnote_1 = require("./footnote");
const headline_1 = __importDefault(require("./headline"));
const html_1 = __importDefault(require("./html"));
const link_1 = __importDefault(require("./link"));
const list_1 = __importStar(require("./list"));
const paragraph_1 = __importDefault(require("./paragraph"));
const section_1 = __importDefault(require("./section"));
const table_1 = require("./table");
const text_1 = require("./text");
exports.default = {
    keyword: keyword_1.default,
    section: section_1.default,
    headline: headline_1.default,
    'text.bold': text_1.bold,
    'text.italic': text_1.italic,
    'text.code': text_1.code,
    'text.verbatim': text_1.verbatim,
    'text.strikeThrough': text_1.strikeThrough,
    'text.underline': text_1.underline,
    paragraph: paragraph_1.default,
    link: link_1.default,
    block: block_1.default,
    list: list_1.default,
    'list.item': list_1.item,
    'list.item.checkbox': list_1.checkbox,
    table: table_1.table,
    'table.row': table_1.tableRow,
    'table.cell': table_1.tableCell,
    footnote: footnote_1.footnote,
    'footnote.reference': footnote_1.footnoteReference,
    hr: ({ h }) => () => h('hr')(),
    drawer: () => () => undefined,
    priority: () => () => undefined,
    html: html_1.default,
};
//# sourceMappingURL=index.js.map