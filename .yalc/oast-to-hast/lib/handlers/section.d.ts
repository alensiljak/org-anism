import { Section } from 'orga';
import { Context, HNode } from '../';
declare const _default: (context: Context) => (node: Section) => HNode;
export default _default;
