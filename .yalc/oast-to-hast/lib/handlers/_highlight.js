var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prismjs_1 = __importDefault(require("prismjs"));
const _load_prism_language_1 = __importDefault(require("./_load-prism-language"));
exports.default = (language, code) => {
    if (!prismjs_1.default.languages[language]) {
        try {
            _load_prism_language_1.default(language);
        }
        catch (e) {
            return code;
        }
    }
    const lang = prismjs_1.default.languages[language];
    const highlighted = prismjs_1.default.highlight(code, lang);
    return highlighted;
};
//# sourceMappingURL=_highlight.js.map