import { Node } from 'unist';
import { Context, HNode } from '../';
export declare type Handler = (context: Context) => (node: Node) => HNode;
declare const _default: {
    [key: string]: Handler;
};
export default _default;
