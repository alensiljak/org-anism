import { Keyword } from 'orga';
import { Context, HNode } from '../';
declare const _default: (context: Context) => (node: Keyword) => HNode;
export default _default;
