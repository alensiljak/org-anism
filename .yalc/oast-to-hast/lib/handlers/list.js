Object.defineProperty(exports, "__esModule", { value: true });
exports.checkbox = exports.item = void 0;
const transform_1 = require("../transform");
exports.default = (context) => (node) => {
    const { h } = context;
    let tagName = node.ordered ? 'ol' : 'ul';
    if (node.children.every(i => i.tag)) {
        tagName = 'dl';
    }
    return h(tagName, node.attributes.attr_html)(...transform_1.all(context)(node.children));
};
const item = (context) => (node) => {
    const { h, u } = context;
    if (node.tag) {
        return h('div')(h('dt')(u('text', node.tag)), h('dd')(...transform_1.all(context)(node.children)));
    }
    return h('li')(...transform_1.all(context)(node.children));
};
exports.item = item;
const checkbox = ({ h }) => (node) => {
    return h('input', {
        type: 'checkbox',
        checked: node.checked,
        disabled: true,
    })();
};
exports.checkbox = checkbox;
//# sourceMappingURL=list.js.map