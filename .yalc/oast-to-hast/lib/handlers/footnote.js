Object.defineProperty(exports, "__esModule", { value: true });
exports.footnote = exports.footnoteReference = void 0;
const transform_1 = require("../transform");
const footnoteReference = ({ h, u }) => (node) => {
    return h('sup', {
        id: `fnr-${node.label}`,
        className: ['footnote-ref'],
        dataLabel: node.label
    })(h('a', { href: `#fn-${node.label}` })(u('text', `[${node.label}]`)));
};
exports.footnoteReference = footnoteReference;
const footnote = (context) => (node) => {
    const { h } = context;
    return h('div', {
        id: `fn-${node.label}`,
        className: ['footnote'],
        dataLabel: node.label,
    })(...transform_1.all(context)(node.children));
};
exports.footnote = footnote;
//# sourceMappingURL=footnote.js.map