Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (context) => (node) => {
    const { u } = context;
    return u('raw', node.value);
};
//# sourceMappingURL=html.js.map