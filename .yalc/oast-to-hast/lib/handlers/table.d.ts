import { Table, TableCell, TableRow } from 'orga';
import { Context, HNode } from '../';
interface TableContext extends Context {
    isHead: boolean;
}
export declare const tableRow: (context: TableContext) => (node: TableRow) => HNode;
export declare const tableCell: (context: TableContext) => (node: TableCell) => HNode;
export declare const table: (context: Context) => (node: Table) => HNode;
export {};
