var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.strikeThrough = exports.underline = exports.verbatim = exports.code = exports.italic = exports.bold = void 0;
const unist_builder_1 = __importDefault(require("unist-builder"));
const wrap = (tagName) => {
    return ({ h }) => (node) => h(tagName)(unist_builder_1.default('text', node.value));
};
exports.bold = wrap('strong');
exports.italic = wrap('i');
exports.code = wrap('code');
exports.verbatim = wrap('code');
exports.underline = wrap('u');
exports.strikeThrough = wrap('del');
//# sourceMappingURL=text.js.map