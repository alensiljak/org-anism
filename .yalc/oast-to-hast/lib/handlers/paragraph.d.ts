import { Paragraph } from 'orga';
import { Context } from '../';
declare const _default: (context: Context) => (node: Paragraph) => import("..").HNode;
export default _default;
