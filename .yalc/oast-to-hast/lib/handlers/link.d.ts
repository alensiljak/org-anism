import { Link } from 'orga';
import { Context, HNode } from '../';
declare const _default: (context: Context) => (node: Link) => HNode;
export default _default;
