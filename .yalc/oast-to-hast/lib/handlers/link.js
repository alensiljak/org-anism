var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mime_1 = __importDefault(require("mime"));
exports.default = (context) => (node) => {
    const { h, u, properties } = context;
    const { value, description } = node;
    const type = mime_1.default.getType(value);
    if (type && type.startsWith('image')) {
        const p = node.parent;
        return h('figure')(h('img', Object.assign({ src: node.value }, properties))(), h('figcaption')(u('text', p.attributes['caption'] || description || '')));
    }
    return h('a', { href: value })(u('text', description || value));
};
//# sourceMappingURL=link.js.map