var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.all = exports.one = void 0;
const unist_builder_1 = __importDefault(require("unist-builder"));
const unknown = (context) => (node) => {
    const p = node;
    if (p && p.children) {
        return context.h('div')(...exports.all(context)(p.children));
    }
    else if ('value' in node) {
        return unist_builder_1.default('text', `${node.value}`);
    }
    return undefined;
};
const one = (context) => (node) => {
    return (context.handlers[node.type] || unknown)(context)(node);
};
exports.one = one;
const all = (context) => (nodes) => {
    return nodes.map(exports.one(context)).filter(Boolean);
};
exports.all = all;
//# sourceMappingURL=transform.js.map