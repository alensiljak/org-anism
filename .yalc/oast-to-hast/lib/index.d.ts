import { Comment, Element, Properties, Root, Text } from 'hast';
import { Document, Section } from 'orga';
import u from 'unist-builder';
import { Handler } from './handlers';
interface Raw extends Literal {
    type: 'raw';
}
export declare type HNode = Element | Comment | Text | Raw;
declare const defaultOptions: {
    excludeTags: string[];
    selectTags: string[];
    highlight: boolean;
    properties: Properties;
    handlers: {
        [key: string]: Handler;
    };
};
export declare type Options = typeof defaultOptions;
declare const defaultContext: {
    h: (tagName: string, properties?: Properties | undefined) => (...children: HNode[]) => HNode;
    u: typeof u;
    excludeTags: string[];
    selectTags: string[];
    highlight: boolean;
    properties: Properties;
    handlers: {
        [key: string]: Handler;
    };
};
export declare type Context = typeof defaultContext;
export { Handler };
declare const _default: (oast: Document | Section, options?: Partial<Options>) => Root;
export default _default;
