var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const unist_builder_1 = __importDefault(require("unist-builder"));
const handlers_1 = __importDefault(require("./handlers"));
const transform_1 = require("./transform");
const defaultOptions = {
    excludeTags: ['noexport'],
    selectTags: [],
    highlight: true,
    properties: {},
    handlers: {},
};
const build = ({ tagName, properties, children = [], }) => {
    return {
        type: 'element',
        tagName,
        properties,
        children: children,
    };
};
const h = (tagName, properties = undefined) => (...children) => {
    return build({
        tagName,
        properties,
        children,
    });
};
const defaultContext = Object.assign(Object.assign({}, defaultOptions), { h,
    u: unist_builder_1.default });
exports.default = (oast, options = {}) => {
    const context = Object.assign(Object.assign(Object.assign({}, defaultContext), options), { handlers: Object.assign(Object.assign({}, handlers_1.default), options.handlers) });
    const eTags = oast.properties['exclude_tags'];
    if (eTags) {
        context.excludeTags = eTags.split(/\s+/).map(t => t.trim()).filter(Boolean);
    }
    const sTags = oast.properties['select_tags'];
    if (sTags) {
        context.selectTags = sTags.split(/\s+/).map(t => t.trim()).filter(Boolean);
    }
    return unist_builder_1.default('root', transform_1.all(context)(oast.children));
};
//# sourceMappingURL=index.js.map