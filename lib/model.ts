/*
Data model
*/

export interface IOrgDoc {
  id: string
  content: string
  title: string | null | undefined
}

export class OrgDoc implements IOrgDoc {
  id: string
  content: string
  title: string | null | undefined

  public constructor() {
    this.id = ''
    this.title = ''
    this.content = ''
  }

  // public constructor(id: string, content: string, title?: string | undefined) {
  //   this.id = id
  //   this.content = content
  //   this.title = title
  // }
}
