/*
 * Data Access Layer
   using Dexie
 */
import Dexie from 'dexie'
import { IOrgDoc, OrgDoc } from './model'

export class OrganismDb extends Dexie {
  docs: Dexie.Table<IOrgDoc, String>

  constructor() {
    super('org-anism')

    // The first version
    // i.e. '/first.org', 'First', '* hola'
    // id is the full virtual path => '/doc.org'
    this.version(0.1).stores({
      docs: '++id,title,content',
    })

    // tables
    this.docs = this.table('docs')

    // mappings
    this.docs.mapToClass(OrgDoc)
  }
}
